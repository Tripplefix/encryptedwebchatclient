// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// import 'bootstrap';
// import 'bootstrap/dist/css/bootstrap.min.css';
import Vue from 'vue';
import VueResource from 'vue-resource';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.css';
import 'vue-material/dist/theme/default.css';
import auth from '@/utils/auth';
import Config from '@/utils/webconfig';
import VueSocketio from './plugins/Vue-Socket.io/Main';
import router from './router';
import App from './App';

Vue.use(VueResource);
Vue.use(VueMaterial);
Vue.use(VueSocketio);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  data() {
    return {
      username: '',
    };
  },
  created() {
    // connect to websocket if logged in
    if (auth.loggedIn() && !this.$socket) {
      this.username = auth.getUsername();
      console.log(`${this.username}, you're still logged in, trying to reconnect...`);
      const params = {
        type: 'login',
        token: {
          username: this.username,
          tokenString: auth.getToken(),
        },
      };
      this.$connectToSocket(`${Config.websocket}?data=${JSON.stringify(params)}`);
      this.$socket.emit('enterChatRoom', JSON.stringify(params));
    }
  },
  sockets: {
    connect() {
      console.log('socket connected');
    },
    disconnect() {
      console.log('socket disconnected');
    },
  },
  components: { App },
  template: '<App/>',
});
