// import ws from '@/utils/ws-handler';

export default {
/*
  login(vm, username, password, cb) {
    cb = arguments[arguments.length - 1];
    if (localStorage.token) {
      if (cb) cb(true);
      this.onChange(true);
      return;
    }
    // vm.$socket.send('some data');

    vm.ws.login(vm, username, password, (res) => {
      if (res.authenticated) {
        localStorage.token = res.token;
        if (cb) cb(true);
        this.onChange(true);
      } else {
        if (cb) cb(false);
        this.onChange(false);
      }
    });
  },
*/
  setLogin(username, token) {
    localStorage.token = token;
    localStorage.username = username;
    this.onChange(true);
  },

  getToken() {
    return localStorage.token;
  },

  getUsername() {
    return localStorage.username;
  },

  logout(cb) {
    localStorage.clear();
    if (cb) cb();
    this.onChange(false);
  },

  loggedIn() {
    return !!localStorage.token;
  },

  onChange() {},
};
