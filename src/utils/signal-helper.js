import libsignal from '../plugins/Signal-Protocol/libsignal-protocol';
import MySignalProtocolStore from '../plugins/Signal-Protocol/signalprotocol-store';

// require('../plugins/Signal-Protocol/libsignal-protocol');
const store = new MySignalProtocolStore();

export default {
  generateKeys() {
    const KeyHelper = libsignal.KeyHelper;

    const registrationId = KeyHelper.generateRegistrationId();
    // Store registrationId somewhere durable and safe.

    KeyHelper.generateIdentityKeyPair().then((identityKeyPair) => {
      // keyPair -> { pubKey: ArrayBuffer, privKey: ArrayBuffer }
      // Store identityKeyPair somewhere durable and safe.
      localStorage.identityKeyPair = identityKeyPair;
    });

    KeyHelper.generatePreKey(keyId).then((preKey) => {
      store.storePreKey(preKey.keyId, preKey.keyPair);
    });

    KeyHelper.generateSignedPreKey(identityKeyPair, keyId).then((signedPreKey) => {
      store.storeSignedPreKey(signedPreKey.keyId, signedPreKey.keyPair);
    });

    // Register preKeys and signedPreKey with the server
  },
};
