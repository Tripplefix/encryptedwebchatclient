export default {
  websocket: 'https://socket.dotacraft.ch/',
  api: 'https://api.dotacraft.ch/',
  websocket_: 'http://localhost:9092/',
  api_: 'http://localhost:4567/',
};
