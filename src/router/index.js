import Vue from 'vue';
import Router from 'vue-router';
import ConversationList from '@/components/ConversationList';
import Conversation from '@/components/Conversation';
import Empty from '@/components/Empty';
import Login from '@/components/Login';
import Register from '@/components/Register';
import auth from '@/utils/auth';

Vue.use(Router);

function requireAuth(to, from, next) {
  if (!auth.loggedIn()) {
    next({
      path: '/login/',
    });
  } else {
    next();
  }
}

function redirectIfLoggedIn(to, from, next) {
  if (auth.loggedIn()) {
    next({
      path: '/',
    });
  } else {
    next();
  }
}

const router = new Router({
  mode: 'history', // enable history mode
  routes: [
    {
      path: '/',
      name: 'Home',
      components: {
        default: Empty,
        conversationList: ConversationList,
      },
      beforeEnter: requireAuth,
    },
    {
      path: '/login/',
      name: 'Login',
      component: Login,
      beforeEnter: redirectIfLoggedIn,
    },
    {
      path: '/register/',
      name: 'Register',
      component: Register,
      beforeEnter: redirectIfLoggedIn,
    },
    {
      path: '/logout/',
      name: 'Logout',
      beforeEnter(to, from, next) {
        auth.logout();
        next('/login/');
      },
    },
    {
      path: '/:username',
      name: 'Conversation',
      components: {
        default: Conversation,
        conversationList: ConversationList,
      },
      beforeEnter: requireAuth,
    },
  ],
});

export default router;
